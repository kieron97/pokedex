#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
//CAPS FOR FUNCTION / STRUCT NAME
/*Making a struct */
typedef struct Pokemon{
// data of the pokemon
  int Dex_Number; // identifier
//can also sort the dex by this
  char Name[24];//longest name Gigantimax_Copperajah
  int Can_Evolve;//if they can evolve they should be pointed to the next pokemon that evolves from it
  char type1[12];// tis one can not be Null has to have a value
  char type2[12]; // for monotype pokemon this will not be needed
  char ability[50];//every pokemon has an ability
  struct Pokemon *next;
}Pokemon;
typedef struct Player{// to give each player their own selection of pokemon
  char Player_name[12];
  int ID; // unique identifier 
  Pokemon pokemon;
  struct Player * next;//allows lists to be made
  
}Player;
typedef struct Pokedex
{
 Pokemon *Pokehead;
 Player *Playerhead;
 int PokeCount;
  int PlayerCount;
}Pokedex;

/*
Declarations
================================================
*/
Pokemon* New_Pokemon(int Dex_Number,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]);
void Sort_Add_Pokemon_To_Dex(Pokemon **Start,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]);
void display_dex (Pokemon *start);
void *Add_Start (Pokemon **Start,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]);
void Capture_Pokemon(Player **Start,char Name_Player[12],int ID,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]);
void *New_player_Pokemon(char Name_Player[12],int ID,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]);
void display_box(Player *start,char Name[12],int ID);
void Display_Players(Player *Start);
void display_pokemon(Pokemon* start ,int dex_Number);
void display_player_details(Player *start , int ID);
/*
END OF DECLARATIONS
=================================================
*/

void main(void) {
Pokedex *Test;
Test->Playerhead = NULL;
Test->Pokehead = NULL;
Pokemon *head = Test->Pokehead;
Player *box = Test->Playerhead;
  //Adding pokemon to dex
  Sort_Add_Pokemon_To_Dex(&head,2,"Ivysaur",1,"Grass","Poison","Overgrow");
  Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,0,"Victini",0,"Fire","Psychic","Victory star");// they are added out of order to make sure the sorting algorithm works
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,3,"Venusaur",0,"Grass","Poison","Overgrow ");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,1,"Bulbasaur",1,"Grass","Poison","Overgrow");// it works in context also as you dont discover the pokedex in order however the pokedex is ordered
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,666,"Vivilion",0,"Bug","Flying","Shield Dust");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,4,"Squirtle",1,"Water","None","Torrent");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,5,"Wartortle",1,"Water","None","Torrent");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,6,"Blastoise",0,"Water","None","Torrent");
   Test->PokeCount++;
   Sort_Add_Pokemon_To_Dex(&head,6,"Blastoise",0,"Water","None","Torrent");
  Sort_Add_Pokemon_To_Dex(&head,7,"Charmander",1,"Fire","None","Blaze");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,8,"Charmeleon",1,"Fire","None","Blaze");
   Test->PokeCount++;
  Sort_Add_Pokemon_To_Dex(&head,425,"Drifloon",1,"Ghost","Flying","Aftermath");
   Test->PokeCount++;
  //end of adding to dex
 
  display_dex(head);// prints out the dex
   display_pokemon(head,2);//prints out dex entry for ivysaur
  //adds pokemon to boxes
  Capture_Pokemon(&box,"Kieron",0,2,"Ivysaur",1,"Grass","Poison","Overgrow");// Scenario 1 I have many of the same pokemon in the box
  Capture_Pokemon(&box,"Kieron",0,2,"Ivysaur",1,"Grass","Poison","Overgrow");
  Capture_Pokemon(&box,"Kieron",0,2,"Ivysaur",1,"Grass","Poison","Overgrow");
  Capture_Pokemon(&box,"Kieron",0,2,"Ivysaur",1,"Grass","Poison","Overgrow");
  Capture_Pokemon(&box,"Kieron",0,2,"Ivysaur",1,"Grass","Poison","Overgrow");
  Test->PlayerCount++;
  Capture_Pokemon(&box,"Steve",1,1,"Bulbasaur",1,"Grass","Poison","Overgrow");
  Capture_Pokemon(&box,"Steve",1,7,"Charmander",1,"Fire","None","Blaze");
    Test->PlayerCount++;
  display_player_details(box,0);
  display_player_details(box,2);
  //End of adding pokemon to boxes
  display_box(box,"Steve",1);
   display_box(box,"Kieron",0);
   Display_Players(box);
}
//FUNCTIONS to be called
//===============================================
// Inserts the data into the Pokemon node
Pokemon* New_Pokemon(int Dex_NumberIn,char NameIn[24],int Can_EvolveIn,char type1In[12],char type2In[12],char abilityIn[50]){
  Pokemon *new_pokemon = NULL;               
  new_pokemon =malloc(sizeof(Pokemon)); // Finds the memory needed for a new pokemon     
  if (new_pokemon != NULL)// checks if the memory space has been created         
  {
    new_pokemon->Dex_Number = Dex_NumberIn;//inserts the pokemons data 
    strcpy(new_pokemon->Name,NameIn);
    new_pokemon->Can_Evolve = Can_EvolveIn;
    strcpy(new_pokemon->type1,type1In);
    strcpy(new_pokemon->type2,type2In);
    strcpy(new_pokemon->ability,abilityIn);
    new_pokemon->next = NULL;// the node exists but nothing points to it in this function and it doesnt point to anything
  }
  
  return new_pokemon;//returns the newly created node to wherever called
}
void *Add_Start (Pokemon **Start,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]){
  Pokemon *Temp = New_Pokemon(Dex_NumberIN,Name,Can_Evolve,type1,type2,ability);// Calls the function to create a node
  Temp->next = *Start;//as the node has no pointers it gives pointers so it doesnt get lost && points the node to the node what was previously at the beginning
  *Start = Temp;//
}
void Sort_Add_Pokemon_To_Dex (Pokemon **Start,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]){
  Pokemon *Temporary = *Start;//makes a copy of start for safety and to traverse the list
  Pokemon *Prev = NULL;// these will be needed to calibrate the pointers
  Pokemon *Future = NULL;//only needed if the node being added is in between 2 existing nodes
  Pokemon *pok = New_Pokemon(Dex_NumberIN,Name,Can_Evolve,type1,type2,ability);// The pokemon being inserted
  if (Temporary == NULL||Dex_NumberIN <= Temporary->Dex_Number){// for empty list or lower dex number
    Add_Start(Start,Dex_NumberIN,Name,Can_Evolve,type1,type2,ability);//adds to the beggining of the list
  }
  else{//if the list isnt empty AND the dex number isnt the lowest in the list
    while (Temporary != NULL && Temporary->Dex_Number <= Dex_NumberIN){//while not at the end of the list AND the nodes dex number is smaller than the New Nodes dex number 
      if (Temporary->Dex_Number == Dex_NumberIN)
      {
        printf("Pokemon %s Already Exists \n",Temporary->Name);
        return;
      }
      
      Prev = Temporary;//to point to the new node after the while loop
      Temporary = Temporary->next; // moves to the next node
      Future = Temporary;//the new node will point towards this after the while loop
    }
    Prev->next = pok;//Points to the New node
    pok->next = Future;//New node points to the future node
  }
  
}
void display_pokemon(Pokemon* start ,int dex_Number){
  Pokemon *Temp = start;
  while(Temp != NULL && Temp->Dex_Number != dex_Number){
    Temp = Temp->next;
  }
  if (Temp-> Dex_Number == dex_Number){
    printf("Pokedex Entry %d", dex_Number);//For Presentation Purposes
  printf("\n");
  printf("========================");//For Presentation Purposes
  printf("\n");
    printf("%s",Temp->Name);//Displays Pokemon name
    printf("\n");
    printf("%s",Temp->type1);//Displays type1
    printf("\n");
    if (Temp->type2 != "None"){//Checks if the pokemon is dual type
      printf("%s",Temp->type2);
      printf("\n");
    }
    printf("%s",Temp->ability);//Displays Ability
    printf("\n");
    if (Temp->Can_Evolve ==1 && Temp->Dex_Number + 1 == Temp->next->Dex_Number)  {
      printf("Evolves into %s",Temp->next->Name);
        printf("\n");
    }
  }
  else{
  printf("ERROR 404 POKEMON NOT FOUND");
    }
   printf("========================");//For Presentation Purposes
     printf("\n");
}
void display_player_details(Player *start , int ID){
  printf("Pokedex Entry %d", ID);//For Presentation Purposes
  printf("\n");
  printf("========================");//For Presentation Purposes
  Player *Temp = start;
  
while (Temp ->next!= NULL && Temp->ID != ID)
{
  Temp = Temp->next;
}

  if(Temp->ID == ID){
    printf("Player Name %s",start->Player_name);
  }
  else{
    printf("ERROR 404 PLayer NOT FOUND");
  }
    printf("========================");//For Presentation Purposes
  printf("\n");
}
void display_dex (Pokemon *start){// start is the beginning of the linked list of nodes
  printf("Pokedex");//For Presentation Purposes
  printf("\n");
  printf("========================");//For Presentation Purposes
  printf("\n");
    while(start != NULL){// While the end of the node has not been reached
    printf("Dex Number: %d\n",start->Dex_Number);//Displays Dex Number
    printf("%s",start->Name);//Displays Pokemon name
    printf("\n");
    printf("%s",start->type1);//Displays type1
    printf("\n");
    if (start->type2 != "None"){//Checks if the pokemon is dual type
      printf("%s",start->type2);
      printf("\n");
    }
    printf("%s",start->ability);//Displays Ability
    printf("\n");
    if (start->Can_Evolve > 0){//checks if the pokemon can evolve
      if(start->next->Dex_Number == start->Dex_Number +1){// checks if the evolution is regestered
        printf("Evolves into next entry");
        printf("\n");
      }
      else{//If it evolves BUT the Pokemon it evolves into has not been found
        printf("Evolution not discovered");
        printf("\n");
      }
    }
    start = start->next;
    printf("========================");
    printf("\n");
  }
}
void Display_Players(Player *Start){
  int x = 0;
  while (Start != NULL){
    if( x == Start->ID){
    printf("%s",strcpy(Start->Player_name,Start->Player_name));
        printf("\n");
    x++;
    }
    Start =Start->next;
  }
}

void *New_player_Pokemon(char Name_Player[12],int IDIN,int Dex_NumberIN,char Pokemon[24],int Can_Evolve,char type1[12],char type2[12],char ability[50]){
  Player *New = NULL;// the new pokemon = nothing for safety
  New =malloc(sizeof(Player)); //allocates the  memory needed for a player
  if (New != NULL)             
  {
    strcpy(New->Player_name,Name_Player);//inserts the players name
    New->ID=IDIN;//inserts the players ID
    New->pokemon.Dex_Number = Dex_NumberIN;//inserts the pokemons dex number 
    strcpy(New->pokemon.Name,Pokemon); // inserts the name of the pokemon
    New->pokemon.Can_Evolve= Can_Evolve;//inserts the evolution code
    strcpy(New->pokemon.type1,type1);// inserts the type one 
    strcpy(New->pokemon.type2,type2);//inserts type 2
    strcpy(New->pokemon.ability,ability);//inserts the ability
    New->next = NULL;//Not pointing to a node for now

  }
  
  return New;//Returns the newly created node
}
void Capture_Pokemon(Player **Start,char Name_Player[12],int ID ,int Dex_NumberIN,char Name[24],int Can_Evolve,char type1[12],char type2[12],char ability[50])
{
  Player *Temp = *Start;// temporary pointer
  Player *Pla = New_player_Pokemon(Name_Player,ID,Dex_NumberIN,Name,Can_Evolve,type1,type2,ability);// makes the pokemon
  Player *Prev = NULL;// To point to the temp while traversing the list
  if (Temp == NULL){//checks if the list is empty and as boxes are in a random order they dont need to be sorted 
  Pla->next = *Start;//Makes the new node point to the beginning
  *Start = Pla;//the begginging is now pla
  }// as it is a box the pokemon do not need to be ordered and a player can have many of the same pokemon
  else{//if the list has items
     while(Temp->next != NULL){//while the list isnt at the end 
       Prev = Temp;//the previos node is the node what was just checked
       Temp = Temp->next;//moving through the list
      }
      Prev = Temp;//the final move
       Temp = Temp->next;// moves the temp to a null valu

      Prev->next = Pla;//points the prev to the new node
  }
}

void display_box(Player *start,char Name[12],int ID){
  printf("%s's Box",Name);//the name of the owner of the box
  printf("\n");
  printf("========================");
  printf("\n");
  while (start != NULL)//while not reached the end of the list
  {

    if(ID == start->ID){// this checks if the pokemon stored is the characters
  
    printf("%i Dex Number",start->pokemon.Dex_Number);//if yes display
    printf("\n");
    printf("%s",start->pokemon.Name);
    printf("\n");
    printf("%s",start->pokemon.type1);
    printf("\n");
    if (start->pokemon.type2 != "None"){//Checks if the pokemon is dual type
      printf("%s",start->pokemon.type2);
      printf("\n");
    }
    printf("Owner %s",start->Player_name);
    printf("\n");
    printf("========================");
    printf("\n");
    }
    start=start->next;
  }
    printf("\n");
}
//END OF FUNCTIONS
//==============================================/
